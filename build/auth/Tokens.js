"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const {
  sign
} = _jsonwebtoken.default;
const getTokenPair = async user => {
  const userData = {
    username: user.username,
    id: user._id,
    role: user.role,
    names: user.names,
    lastnames: user.lastnames
  };
  const accessToken = sign(userData, process.env.JWT_ACCESS_SECRET, {
    expiresIn: '1d'
  });
  const refreshToken = sign({
    id: user._id
  }, process.env.JWT_REFRESH_SECRET, {
    expiresIn: '7d'
  });
  return {
    refreshToken,
    accessToken
  };
};
var _default = exports.default = getTokenPair;
//# sourceMappingURL=Tokens.js.map