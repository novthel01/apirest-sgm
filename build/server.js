"use strict";

var _express = _interopRequireWildcard(require("express"));
var dotenv = _interopRequireWildcard(require("dotenv"));
var _routers = _interopRequireDefault(require("./routers"));
var _compression = _interopRequireDefault(require("compression"));
var _cors = _interopRequireDefault(require("cors"));
var _db = require("./db/db");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
dotenv.config();
const app = (0, _express.default)();
const port = process.env.PORT || 8000;

// configuracion middleware

app.use((0, _cors.default)());
app.use((0, _express.json)());
app.use((0, _express.urlencoded)({
  extended: false
}));
app.use((0, _compression.default)());

// establish connection to mongoDB
(0, _db.connectToMongoDB)();
(0, _routers.default)(app);

// configure mongoDB connection closure
(0, _db.subscribeCloseMongoDB)();
app.listen(port, () => {
  console.log(`Servidor esta corriendo en el puerto ${port}`);
});
//# sourceMappingURL=server.js.map