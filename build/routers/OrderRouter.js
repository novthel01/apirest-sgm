"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _express = _interopRequireDefault(require("express"));
var _OrderModels = _interopRequireDefault(require("../models/OrderModels.js"));
var _UserModel = _interopRequireDefault(require("../models/UserModel.js"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const orderRouter = _express.default.Router();

/**
 * Listar todos las ordenes de la base de datos
 */
orderRouter.get('/all', (req, res) => {
  _OrderModels.default.find({}).then(order => {
    if (order.length !== 0) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Orders found',
        data: order
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Order not found'
      });
    }
  }).catch(error => {
    console.log(error);
    return res.status(500).send({
      state: 'error',
      msj: 'ERROR: Internal Server Error'
    });
  });
});

/**
 * Consulta todos las ordenes de la base de datos por numero de orden
 */
orderRouter.post('/all/ordernumber', (req, res) => {
  const ordernumber = req.body.ordernumber;
  _OrderModels.default.findOne({
    ordernumber
  }).then(order => {
    if (order !== null) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Order found',
        data: order
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Order not found'
      });
    }
  }).catch(error => {
    console.log(error);
    return res.status(500).send({
      state: 'error',
      msj: 'ERROR: Internal Server Error'
    });
  });
});

/**
 * Consulta todos las ordenes de la base de datos por usuario asigado a ejecutarla
 */
orderRouter.post('/all/assignment', (req, res) => {
  const {
    asignateto
  } = req.body;
  _OrderModels.default.find({
    asignateto
  }).then(order => {
    if (order !== null) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Order found',
        data: order
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Order not found'
      });
    }
  }).catch(error => {
    console.log(error);
    return res.status(500).send({
      state: 'error',
      msj: 'ERROR: Internal Server Error'
    });
  });
});

/**
 * Crear nueva orden
 */
orderRouter.post('/new', (req, res) => {
  const body = req.body;
  const newOrder = new _OrderModels.default(body);
  newOrder.save().then(() => {
    res.status(201).send({
      state: 'Created',
      msj: 'Order saved successfully'
    });
  }).catch(error => {
    console.log(error);
    res.status(400).send({
      state: 'Bad request',
      msj: 'ERROR: Bad request'
    });
  });
});

/**
 * consulta una orden especifica. recibe como parametro el numero de la orden.
 */
orderRouter.post('/all/:id', async (req, res) => {
  const {
    id
  } = req.params;
  const order = await _OrderModels.default.findById(id);
  const userAssigneto = await _UserModel.default.findById(order.asignateto);
  _UserModel.default.findById(order.createby).then(createby => {
    const orderComplete = {
      order,
      userAssigneto,
      createby
    };
    return res.status(200).send({
      state: 'Ok',
      msj: 'Order found',
      data: orderComplete
    });
  }).catch(error => {
    console.log(error);
    return res.status(500).send({
      state: 'error',
      msj: 'ERROR: Internal Server Error'
    });
  });
});

/**
 * Edita una orden especifica. recibe como parametro el id de la orden.
 */
orderRouter.put('/edit/:id', (req, res) => {
  const {
    id
  } = req.params;
  const body = req.body.data;
  _OrderModels.default.findByIdAndUpdate(id, body, {
    new: true
  }).then(order => {
    if (order) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Order edit successfully'
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Order not found'
      });
    }
  }).catch(error => {
    console.log(error);
    res.status(400).send({
      state: 'error',
      msj: 'malformatted id'
    });
  });
});

/**
 * Elimina una orden especifica. recibe como parametro el id de la orden.
 */
orderRouter.delete('/delete/:id', (req, res) => {
  _OrderModels.default.findByIdAndDelete(req.params.id).then(order => {
    if (order) {
      return res.status(204).send({
        state: 'Not Content',
        msj: 'Order delete successfully'
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Order not found'
      });
    }
  }).catch(error => {
    console.log(error);
    return res.status(502).send({
      state: 'error',
      msj: 'ERROR: Bad Gateway'
    });
  });
});
var _default = exports.default = orderRouter;
//# sourceMappingURL=OrderRouter.js.map