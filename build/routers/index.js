"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _CompanyRouter = _interopRequireDefault(require("./CompanyRouter.js"));
var _NotificationRouter = _interopRequireDefault(require("./NotificationRouter.js"));
var _OrderRouter = _interopRequireDefault(require("./OrderRouter.js"));
var _RequestHelpRouter = _interopRequireDefault(require("./RequestHelpRouter.js"));
var _UserRouter = _interopRequireDefault(require("./UserRouter.js"));
var _LoginRouter = _interopRequireDefault(require("./LoginRouter.js"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const allRouters = app => {
  app.use('/users', _UserRouter.default);
  app.use('/orders', _OrderRouter.default);
  app.use('/request-maintenance', _NotificationRouter.default);
  app.use('/request-help', _RequestHelpRouter.default);
  app.use('/company', _CompanyRouter.default);
  app.use('/', _LoginRouter.default);
};
var _default = exports.default = allRouters;
//# sourceMappingURL=index.js.map