"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _express = _interopRequireDefault(require("express"));
var _RequestHelpModels = _interopRequireDefault(require("../models/RequestHelpModels.js"));
var _UserModel = _interopRequireDefault(require("../models/UserModel.js"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const requestHelpRouter = _express.default.Router();

/**
 * Crear nueva solicitud de ayuda
 */
requestHelpRouter.post('/new', (req, res) => {
  const body = req.body;
  const newRequestHelp = new _RequestHelpModels.default(body);
  newRequestHelp.save().then(() => {
    res.status(201).send({
      state: 'Created',
      msj: 'Request saved successfully'
    });
  }).catch(error => {
    console.log(error);
    res.status(400).send({
      state: 'Bad request',
      msj: 'ERROR: Bad request'
    });
  });
});

/**
 * Listar todos las solicitudes de ayuda de la base de datos
 */
requestHelpRouter.get('/all', (req, res) => {
  _RequestHelpModels.default.find({}).then(request => {
    if (request !== null) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Request found',
        data: request
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: ' Request not found'
      });
    }
  }).catch(error => {
    console.log(error);
    return res.status(500).send({
      state: 'error',
      msj: 'ERROR: Internal Server Error'
    });
  });
});

/**
 * Consultar una solicitud especifica. recibe como parametro el id
 */
requestHelpRouter.get('/all/:id', (req, res) => {
  _RequestHelpModels.default.findById(req.params.id).then(request => {
    if (request) {
      _UserModel.default.findById(request.requestby).then(userInfo => {
        const requestComplete = {
          request,
          userInfo
        };
        return res.status(200).send({
          state: 'Ok',
          msj: 'Request found',
          data: requestComplete
        });
      }).catch(error => {
        console.log(error);
        return res.status(500).send({
          state: 'error',
          msj: 'ERROR: Internal Server Error'
        });
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Request not found'
      });
    }
  }).catch(error => {
    console.log(error);
    res.status(400).send({
      state: 'error',
      msj: 'malformatted id'
    });
  });
});

/**
 * muestra las solicitud  de la base de datos por prioridad. recibe como parametro la prioridad buscada
 */
requestHelpRouter.post('/all/priority', (req, res) => {
  const {
    priority
  } = req.body;
  _RequestHelpModels.default.find({
    priority
  }).then(request => {
    if (request !== null) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Request found',
        data: request
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Request not found'
      });
    }
  }).catch(error => {
    console.log(error);
    return res.status(500).send({
      state: 'error',
      msj: 'ERROR: Internal Server Error'
    });
  });
});

/**
 * Edita una solicitud de ayuda especifica. recibe como parametro el id de la solicitud.
 */
requestHelpRouter.put('/edit/:id', (req, res) => {
  const body = req.body.data;
  _RequestHelpModels.default.findByIdAndUpdate(req.params.id, body, {
    new: true
  }).then(request => {
    if (request) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Request edit successfully'
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Request not found'
      });
    }
  }).catch(error => {
    console.log(error);
    res.status(400).send({
      state: 'error',
      msj: 'malformatted id'
    });
  });
});
var _default = exports.default = requestHelpRouter;
//# sourceMappingURL=RequestHelpRouter.js.map