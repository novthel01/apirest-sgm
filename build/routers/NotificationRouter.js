"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _express = _interopRequireDefault(require("express"));
var _NotificationsModel = _interopRequireDefault(require("../models/NotificationsModel.js"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const notificationRouter = _express.default.Router();

/**
 * Crear nueva Notificacion
 */
notificationRouter.post('/new', (req, res) => {
  const body = req.body;
  const newNotification = new _NotificationsModel.default(body);
  newNotification.save().then(() => {
    res.status(201).send({
      state: 'Created',
      msj: 'Notification saved successfully'
    });
  }).catch(error => {
    console.log(error);
    res.status(400).send({
      state: 'Bad request',
      msj: 'ERROR: Bad request'
    });
  });
});

/**
 * Listar todos las Notificaciones de la base de datos
 */
notificationRouter.get('/all', (req, res) => {
  _NotificationsModel.default.find({}).then(notification => {
    if (notification.length !== 0) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Notification found',
        data: notification
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: ' Notification not found'
      });
    }
  }).catch(error => {
    console.log(error);
    return res.status(500).send({
      state: 'error',
      msj: 'ERROR: Internal Server Error'
    });
  });
});

/**
 * Edita una Notificacion especifica. recibe como parametro el id de la notificacion.
 */
notificationRouter.put('/edit/:id', (req, res) => {
  const body = req.body.data;
  _NotificationsModel.default.findByIdAndUpdate(req.params.id, body, {
    new: true
  }).then(notification => {
    if (notification) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Notification edit successfully'
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Notification not found'
      });
    }
  }).catch(error => {
    console.log(error);
    res.status(400).send({
      state: 'error',
      msj: 'malformatted id'
    });
  });
});

/**
 * Consultar una Notificacion especifica. recibe como parametro el id
 */
notificationRouter.get('/all/:id', (req, res) => {
  _NotificationsModel.default.findById(req.params.id).then(notification => {
    if (notification) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Notification found',
        data: notification
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Notification not found'
      });
    }
  }).catch(error => {
    console.log(error);
    res.status(400).send({
      state: 'error',
      msj: 'malformatted id'
    });
  });
});
var _default = exports.default = notificationRouter;
//# sourceMappingURL=NotificationRouter.js.map