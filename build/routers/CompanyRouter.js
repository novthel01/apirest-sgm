"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _express = _interopRequireDefault(require("express"));
var _CompanyModel = _interopRequireDefault(require("../models/CompanyModel.js"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const companyRouter = _express.default.Router();

/**
 * Ingresa la informacion de la compañia.
 * la informacion de la compañia se encuentra en una coleccion diferente de la base de datos
 * informacion sensible
 */
companyRouter.post('/new', (req, res) => {
  const body = req.body;
  const newUser = new _CompanyModel.default(body);
  newUser.save().then(() => {
    res.status(201).send({
      state: 'Created',
      msj: 'User saved successfully'
    });
  }).catch(error => {
    console.log(error);
    if (error.code === 11000) {
      return res.status(400).send({
        state: 'bad request',
        msj: 'User not disponible'
      });
    } else {
      res.status(500).send({
        state: 'Internal Server Error',
        msj: 'ERROR: Internal Server Error'
      });
    }
  });
});

/**
 * Muestra la informacion que se utilizara para identificar la compañia en la pagina web (logo, nombre)
 */
companyRouter.get('/all', (req, res) => {
  _CompanyModel.default.findOne().then(info => {
    if (info !== null) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Info found',
        data: info
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Info not found'
      });
    }
  }).catch(error => {
    console.log(error);
    return res.status(500).send({
      state: 'error',
      msj: 'ERROR: Internal Server Error'
    });
  });
});

/**
 * Editar la informacion de la compañia. informacion sensible
 */
companyRouter.put('/edit', (req, res) => {
  const body = req.body.data;
  const id = body._id;
  _CompanyModel.default.findByIdAndUpdate(id, body, {
    new: true
  }).then(info => {
    if (info) {
      return res.status(200).send({
        state: 'Ok',
        msj: 'Info edit successfully'
      });
    } else {
      return res.status(404).send({
        state: 'Not found',
        msj: 'Info not found'
      });
    }
  }).catch(error => {
    console.log(error);
    res.status(400).send({
      state: 'error',
      msj: 'malformatted id'
    });
  });
});
var _default = exports.default = companyRouter;
//# sourceMappingURL=CompanyRouter.js.map