"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _bcrypt = require("bcrypt");
var _express = _interopRequireDefault(require("express"));
var _Tokens = _interopRequireDefault(require("../auth/Tokens.js"));
var _UserModel = _interopRequireDefault(require("../models/UserModel.js"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const loginRouter = _express.default.Router();

/**
 * Login de usuario. recibe como parametros el nombre de usuario y la contraseña
 */
loginRouter.post('/login', async (req, res) => {
  const user = await _UserModel.default.findOne({
    username: req.body.username
  });
  if (!user) {
    return res.status(401).send({
      state: 'Unauthorized',
      msj: 'Username or password incorrect'
    });
  }
  const passwordVerify = await (0, _bcrypt.compare)(req.body.password, user.password);
  if (!passwordVerify) {
    return res.status(401).send({
      state: 'Unauthorized',
      msj: 'Username or password incorrect'
    });
  }
  try {
    const {
      accessToken
    } = await (0, _Tokens.default)(user);
    const {
      role
    } = user;
    res.status(200).send({
      state: 'Ok',
      msj: 'user has logged in',
      accessToken,
      url: role !== 'operator' ? '/dashboard' : '/notification'
    });
  } catch (error) {
    console.log(error);
  }
});
var _default = exports.default = loginRouter;
//# sourceMappingURL=LoginRouter.js.map