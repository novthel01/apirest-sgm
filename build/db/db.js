"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.subscribeCloseMongoDB = exports.connectToMongoDB = exports.closeMongoDB = void 0;
var _mongoose = _interopRequireDefault(require("mongoose"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
const connectToMongoDB = async () => {
  try {
    console.log('connecting to db...');
    _mongoose.default.set('strictQuery', false);
    _mongoose.default.connect(process.env.MONGODB_URI, {});
    console.log('Connected to the database');
  } catch (e) {
    console.log('database connection error');
    console.log(e);
  }
};
exports.connectToMongoDB = connectToMongoDB;
const closeMongoDB = async () => {
  if (_mongoose.default.connection) {
    try {
      console.log('closing connection...');
      _mongoose.default.connection.close();
      console.log('closed connection');
    } catch (e) {
      console.log('error closing connection');
      console.log(e);
    }
  }
};
exports.closeMongoDB = closeMongoDB;
const subscribeCloseMongoDB = () => {
  process.on('exit', closeMongoDB);
  process.on('SIGINT', closeMongoDB);
  process.on('SIGTERM', closeMongoDB);
  process.on('SIGKILL', closeMongoDB);
  process.on('uncaughtException', closeMongoDB);
};
exports.subscribeCloseMongoDB = subscribeCloseMongoDB;
//# sourceMappingURL=db.js.map