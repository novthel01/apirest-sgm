"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _mongoose = _interopRequireWildcard(require("mongoose"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
const requestHelpSchema = new _mongoose.Schema({
  requestby: {
    type: _mongoose.Schema.ObjectId,
    ref: 'users'
  },
  status: {
    type: String,
    required: [true, 'Priority is required'],
    default: 'pending'
  },
  priority: {
    type: String,
    required: [true, 'Priority is required'],
    max: [50, 'Priority must have a maximum of 50 characters.']
  },
  descriptionproblem: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.'],
    required: [true, 'Description is required']
  },
  requirementdate: {
    type: Date,
    required: [true, 'Date is required'],
    default: new Date()
  }
});
const RequestHelpModel = _mongoose.default.model('requestHelp', requestHelpSchema);
var _default = exports.default = RequestHelpModel;
//# sourceMappingURL=RequestHelpModels.js.map