"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _bcrypt = require("bcrypt");
var _mongoose = _interopRequireWildcard(require("mongoose"));
var _Counter = require("./Counter");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
const userSchema = new _mongoose.Schema({
  code: {
    type: Number,
    unique: true,
    required: [true, 'Code N° is required']
  },
  names: {
    type: String,
    required: [true, 'name is required'],
    max: [100, 'name must have a maximum of 100 characters.']
  },
  lastnames: {
    type: String,
    required: [true, 'lastnames is required'],
    max: [100, 'lastnames must have a maximum of 100 characters.']
  },
  position: {
    type: String,
    required: [true, 'position is required'],
    max: [150, 'position must have a maximum of 150 characters.']
  },
  area: {
    type: String,
    required: [true, 'area is required'],
    max: [100, 'area must have a maximum of 100 characters.']
  },
  phone: {
    type: Number,
    required: [true, 'Phone is required']
  },
  email: {
    type: String,
    required: [true, 'Email is required']
  },
  username: {
    type: String,
    required: [true, 'username is required'],
    max: [24, 'username must have a maximum of 24 characters.'],
    min: [3, 'username must have a minimum of 3 characters'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'Password is required']
  },
  role: {
    type: String,
    required: [true, 'Role is required']
  }
});
userSchema.pre('save', async function (next) {
  const doc = this;
  if (!doc.isModified('password')) return next();
  const salt = await (0, _bcrypt.genSalt)(+process.env.BCRYPT_ROUNDS);
  doc.password = await (0, _bcrypt.hash)(doc.password, salt);
  await (0, _Counter.getNextSequenceValue)('user.id').then(async counter => {
    if (!counter) {
      await (0, _Counter.inserCounter)('user.id').then(counter => {
        doc.code = counter;
        next();
      }).catch(error => next(error));
    } else {
      doc.code = counter;
      next();
    }
  }).catch(error => next(error));
  next();
});
const UserModel = _mongoose.default.model('user', userSchema);
var _default = exports.default = UserModel;
//# sourceMappingURL=UserModel.js.map