"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _mongoose = _interopRequireWildcard(require("mongoose"));
var _Counter = require("./Counter.js");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
const notificationSchema = new _mongoose.Schema({
  notificationnumber: {
    type: Number,
    unique: true,
    required: [true, 'Order N° is required']
  },
  requestby: {
    type: String,
    required: [true, 'requestby is required']
  },
  area: {
    type: String,
    required: [true, 'Area is required'],
    max: [100, 'Area must have a maximum of 100 characters.']
  },
  areasupervisor: {
    type: String,
    required: [true, 'Area supervisor is required'],
    max: [150, 'Area supervisor must have a maximum of 150 characters.']
  },
  requirementdate: {
    type: Date,
    required: [true, 'Date is required'],
    default: new Date()
  },
  machine: {
    type: String,
    required: [true, 'Machine is required'],
    max: [200, 'Machine must have a maximum of 200 characters.']
  },
  priority: {
    type: String,
    required: [true, 'Priority is required'],
    max: [50, 'Priority must have a maximum of 50 characters.']
  },
  status: {
    type: String,
    required: [true, 'Status is required'],
    default: 'pending'
  },
  descriptionproblem: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.'],
    required: [true, 'Description is required']
  }
});
notificationSchema.pre('save', function (next) {
  const doc = this;
  (0, _Counter.getNextSequenceValue)('notification.id').then(counter => {
    if (!counter) {
      (0, _Counter.inserCounter)('notification.id').then(counter => {
        doc.notificationnumber = counter;
        next();
      }).catch(error => next(error));
    } else {
      doc.notificationnumber = counter;
      next();
    }
  }).catch(error => next(error));
});
const NotificationModel = _mongoose.default.model('notification', notificationSchema);
var _default = exports.default = NotificationModel;
//# sourceMappingURL=NotificationsModel.js.map