"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _mongoose = _interopRequireWildcard(require("mongoose"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
const companySchema = new _mongoose.Schema({
  names: {
    type: String,
    required: [true, 'name is required'],
    max: [100, 'name must have a maximum of 100 characters.']
  },
  lastnames: {
    type: String,
    required: [true, 'lastnames is required'],
    max: [100, 'lastnames must have a maximum of 100 characters.']
  },
  docUser: {
    type: Number,
    required: [true, 'Document is required']
  },
  phone: {
    type: Number,
    required: [true, 'Phone is required']
  },
  position: {
    type: String,
    required: [true, 'position is required'],
    max: [150, 'position must have a maximum of 150 characters.']
  },
  email: {
    type: String,
    required: [true, 'Email is required']
  },
  businessName: {
    type: String,
    required: [true, 'Name is required'],
    unique: true
  },
  nit: {
    type: Number,
    required: [true, 'Nit is required']
  },
  foundation: {
    type: Date,
    required: [true, 'Date is required']
  },
  sector: {
    type: String,
    required: [true, 'name is required'],
    max: [100, 'name must have a maximum of 100 characters.']
  },
  companyEmail: {
    type: String,
    required: [true, 'Email is required']
  },
  companyPhone: {
    type: Number,
    required: [true, 'Phone is required']
  },
  companyCellPhone: {
    type: Number,
    required: [true, 'Phone is required']
  },
  authorize: {
    type: Boolean,
    default: false
  }
});
const CompanyModel = _mongoose.default.model('company', companySchema);
var _default = exports.default = CompanyModel;
//# sourceMappingURL=CompanyModel.js.map