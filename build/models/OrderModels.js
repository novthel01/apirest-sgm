"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _mongoose = _interopRequireWildcard(require("mongoose"));
var _Counter = require("./Counter.js");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
const orderSchema = new _mongoose.Schema({
  ordernumber: {
    type: Number,
    unique: true,
    required: [true, 'Order N° is required']
  },
  priority: {
    type: String,
    required: [true, 'Priority is required'],
    max: [50, 'Priority must have a maximum of 50 characters.']
  },
  requestby: {
    type: String,
    required: [true, 'Request by is required'],
    max: [150, 'Request by must have a maximum of 150 characters.']
  },
  area: {
    type: String,
    required: [true, 'Area is required'],
    max: [100, 'Area must have a maximum of 100 characters.']
  },
  activity: {
    type: String,
    required: [true, 'Activity is required'],
    max: [300, 'Activity must have a maximum of 300 characters.']
  },
  createby: {
    type: _mongoose.Schema.ObjectId,
    ref: 'users'
  },
  asignateto: {
    type: _mongoose.Schema.ObjectId,
    ref: 'users'
  },
  spareparts: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.']
  },
  comments: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.']
  },
  ordercreationdate: {
    type: Date,
    required: [true, 'Date is required']
  },
  startdate: {
    type: Date,
    required: [true, 'Date is required']
  },
  enddate: {
    type: Date
  },
  descriptionproblem: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.'],
    required: [true, 'Description is required']
  },
  areasupervisor: {
    type: String,
    required: [true, 'Area supervisor is required'],
    max: [150, 'Area supervisor must have a maximum of 150 characters.']
  },
  machine: {
    type: String,
    required: [true, 'Machine is required'],
    max: [250, 'Machine must have a maximum of 250 characters.']
  },
  requirementdate: {
    type: Date,
    required: [true, 'Date is required']
  },
  orderstatus: {
    type: String,
    required: [true, 'Status is required'],
    default: 'pending'
  }
});
orderSchema.pre('save', function (next) {
  const doc = this;
  (0, _Counter.getNextSequenceValue)('order.id').then(counter => {
    if (!counter) {
      (0, _Counter.inserCounter)('order.id').then(counter => {
        doc.ordernumber = counter;
        next();
      }).catch(error => next(error));
    } else {
      doc.ordernumber = counter;
      console.log(doc.ordernumber);
      next();
    }
  }).catch(error => next(error));
});
const OrderModel = _mongoose.default.model('order', orderSchema);
var _default = exports.default = OrderModel;
//# sourceMappingURL=OrderModels.js.map