"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.inserCounter = exports.getNextSequenceValue = exports.Counter = void 0;
var _mongoose = _interopRequireWildcard(require("mongoose"));
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function (e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != typeof e && "function" != typeof e) return { default: e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n.default = e, t && t.set(e, n), n; }
const CounterSchema = new _mongoose.Schema({
  _id: {
    type: String,
    required: true
  },
  seq: {
    type: Number,
    default: 0
  }
});
const Counter = exports.Counter = _mongoose.default.model('counter', CounterSchema);
const getNextSequenceValue = seqName => {
  return new Promise((resolve, reject) => {
    Counter.findByIdAndUpdate({
      _id: seqName
    }, {
      $inc: {
        seq: 1
      }
    }).then(function (counter) {
      if (counter) {
        resolve(counter.seq + 1);
      } else {
        resolve(null);
      }
    }).catch(function (error) {
      if (error) {
        reject(error);
      }
    });
  });
};
exports.getNextSequenceValue = getNextSequenceValue;
const inserCounter = seqName => {
  const newCounter = new Counter({
    _id: seqName,
    seq: 1000
  });
  return new Promise((resolve, reject) => {
    newCounter.save().then(data => {
      resolve(data.seq);
    }).catch(error => reject(error));
  });
};
exports.inserCounter = inserCounter;
//# sourceMappingURL=Counter.js.map