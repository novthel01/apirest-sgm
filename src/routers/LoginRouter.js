import { compare } from 'bcrypt';
import express from 'express';
import getTokenPair from '../auth/Tokens.js';
import UserModel from '../models/UserModel.js';

const loginRouter = express.Router();

/**
 * Login de usuario. recibe como parametros el nombre de usuario y la contraseña
 */
loginRouter.post('/login', async (req, res) => {
  const user = await UserModel.findOne({ username: req.body.username });
  if (!user) {
    return res
      .status(401)
      .send({ state: 'Unauthorized', msj: 'Username or password incorrect' });
  }
  const passwordVerify = await compare(req.body.password, user.password);
  if (!passwordVerify) {
    return res
      .status(401)
      .send({ state: 'Unauthorized', msj: 'Username or password incorrect' });
  }
  try {
    const { accessToken } = await getTokenPair(user);
    const { role } = user;
    res.status(200).send({
      state: 'Ok',
      msj: 'user has logged in',
      accessToken,
      url: role !== 'operator' ? '/dashboard' : '/notification',
    });
  } catch (error) {
    console.log(error);
  }
});

export default loginRouter;
