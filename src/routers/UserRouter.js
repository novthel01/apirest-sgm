import express from 'express';
import UserModel from '../models/UserModel.js';

const userRouter = express.Router();

/**
 * Listar todos los usuarios de la base de datos
 */
userRouter.get('/all', (req, res) => {
  UserModel.find({})
    .then((user) => {
      if (user.length !== 0) {
        return res
          .status(200)
          .send({ state: 'Ok', msj: 'Users found', data: user });
      } else {
        return res.status(404).send({ state: 'error', msj: 'User not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(500)
        .send({ state: 'error', msj: 'ERROR: Internal Server Error' });
    });
});

/**
 * Consulta todos los usuarios de la base de datos por rol
 */
userRouter.post('/role', (req, res) => {
  const { role } = req.body;

  UserModel.find({ role })
    .then((user) => {
      if (user.length !== 0) {
        return res
          .status(200)
          .send({ state: 'Ok', msj: 'Users found', data: user });
      } else {
        return res
          .status(404)
          .send({ state: 'Not found', msj: 'User not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(500)
        .send({ state: 'error', msj: 'ERROR: Internal Server Error' });
    });
});

/**
 * Crea un nuevo usuario
 */
userRouter.post('/new', (req, res) => {
  const body = req.body;
  const newUser = new UserModel(body);
  newUser
    .save()
    .then(() => {
      res
        .status(201)
        .send({ state: 'Created', msj: 'User saved successfully' });
    })
    .catch((error) => {
      console.log(error);
      if (error.code === 11000) {
        return res.status(400).send({
          state: 'bad request',
          msj: 'User not disponible',
        });
      } else {
        res.status(500).send({
          state: 'Internal Server Error',
          msj: 'ERROR: Internal Server Error',
        });
      }
    });
});

/**
 * Eliminar usuario. recibe como parametro el id del usuario
 */
userRouter.delete('/delete/:id', (req, res) => {
  UserModel.findByIdAndDelete(req.params.id)
    .then((user) => {
      if (user) {
        return res
          .status(204)
          .send({ state: 'Not Content', msj: 'User delete successfully' });
      } else {
        return res
          .status(404)
          .send({ state: 'Not found', msj: 'User not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(502)
        .send({ state: 'error', msj: 'ERROR: Bad Gateway' });
    });
});

/**
 * Edita un usuario especifico. recibe como parametro el id del usuario.
 */
userRouter.put('/edit/:id', (req, res) => {
  UserModel.findByIdAndUpdate(req.params.id, req.body, { new: true })
    .then((user) => {
      if (user) {
        return res
          .status(200)
          .send({ state: 'Ok', msj: 'User edit successfully' });
      } else {
        return res
          .status(404)
          .send({ state: 'Not found', msj: 'User not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      res.status(400).send({ state: 'error', msj: 'malformatted id' });
    });
});

/**
 * Consulta un usuario especifico. recibe como parametro el id
 */
userRouter.get('/:id', (req, res) => {
  const { id } = req.params;

  UserModel.findById(id)
    .then((user) => {
      if (user) {
        return res
          .status(200)
          .send({ state: 'Ok', msj: 'User found successfully', data: user });
      } else {
        return res
          .status(404)
          .send({ state: 'Not found', msj: 'User not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(500)
        .send({ state: 'error', msj: 'ERROR: Internal Server Error' });
    });
});

export default userRouter;
