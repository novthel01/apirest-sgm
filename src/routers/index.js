import companyRouter from './CompanyRouter.js';
import notificationRouter from './NotificationRouter.js';
import orderRouter from './OrderRouter.js';
import requestHelpRouter from './RequestHelpRouter.js';
import userRouter from './UserRouter.js';
import loginRouter from './LoginRouter.js';

const allRouters = (app) => {
  app.use('/users', userRouter);
  app.use('/orders', orderRouter);
  app.use('/request-maintenance', notificationRouter);
  app.use('/request-help', requestHelpRouter);
  app.use('/company', companyRouter);
  app.use('/', loginRouter);
};

export default allRouters;
