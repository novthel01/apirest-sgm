import express from 'express';
import OrderModel from '../models/OrderModels.js';
import UserModel from '../models/UserModel.js';

const orderRouter = express.Router();

/**
 * Listar todos las ordenes de la base de datos
 */
orderRouter.get('/all', (req, res) => {
  OrderModel.find({})
    .then((order) => {
      if (order.length !== 0) {
        return res
          .status(200)
          .send({ state: 'Ok', msj: 'Orders found', data: order });
      } else {
        return res
          .status(404)
          .send({ state: 'Not found', msj: 'Order not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(500)
        .send({ state: 'error', msj: 'ERROR: Internal Server Error' });
    });
});

/**
 * Consulta todos las ordenes de la base de datos por numero de orden
 */
orderRouter.post('/all/ordernumber', (req, res) => {
  const ordernumber = req.body.ordernumber;
  OrderModel.findOne({ ordernumber })
    .then((order) => {
      if (order !== null) {
        return res
          .status(200)
          .send({ state: 'Ok', msj: 'Order found', data: order });
      } else {
        return res
          .status(404)
          .send({ state: 'Not found', msj: 'Order not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(500)
        .send({ state: 'error', msj: 'ERROR: Internal Server Error' });
    });
});

/**
 * Consulta todos las ordenes de la base de datos por usuario asigado a ejecutarla
 */
orderRouter.post('/all/assignment', (req, res) => {
  const { asignateto } = req.body;

  OrderModel.find({ asignateto })
    .then((order) => {
      if (order !== null) {
        return res
          .status(200)
          .send({ state: 'Ok', msj: 'Order found', data: order });
      } else {
        return res
          .status(404)
          .send({ state: 'Not found', msj: 'Order not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(500)
        .send({ state: 'error', msj: 'ERROR: Internal Server Error' });
    });
});

/**
 * Crear nueva orden
 */
orderRouter.post('/new', (req, res) => {
  const body = req.body;

  const newOrder = new OrderModel(body);
  newOrder
    .save()
    .then(() => {
      res
        .status(201)
        .send({ state: 'Created', msj: 'Order saved successfully' });
    })
    .catch((error) => {
      console.log(error);
      res.status(400).send({
        state: 'Bad request',
        msj: 'ERROR: Bad request',
      });
    });
});

/**
 * consulta una orden especifica. recibe como parametro el numero de la orden.
 */
orderRouter.post('/all/:id', async (req, res) => {
  const { id } = req.params;

  const order = await OrderModel.findById(id);
  const userAssigneto = await UserModel.findById(order.asignateto);

  UserModel.findById(order.createby)
    .then((createby) => {
      const orderComplete = { order, userAssigneto, createby };
      return res.status(200).send({
        state: 'Ok',
        msj: 'Order found',
        data: orderComplete,
      });
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(500)
        .send({ state: 'error', msj: 'ERROR: Internal Server Error' });
    });
});

/**
 * Edita una orden especifica. recibe como parametro el id de la orden.
 */
orderRouter.put('/edit/:id', (req, res) => {
  const { id } = req.params;
  const body = req.body.data;

  OrderModel.findByIdAndUpdate(id, body, { new: true })
    .then((order) => {
      if (order) {
        return res
          .status(200)
          .send({ state: 'Ok', msj: 'Order edit successfully' });
      } else {
        return res
          .status(404)
          .send({ state: 'Not found', msj: 'Order not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      res.status(400).send({ state: 'error', msj: 'malformatted id' });
    });
});

/**
 * Elimina una orden especifica. recibe como parametro el id de la orden.
 */
orderRouter.delete('/delete/:id', (req, res) => {
  OrderModel.findByIdAndDelete(req.params.id)
    .then((order) => {
      if (order) {
        return res
          .status(204)
          .send({ state: 'Not Content', msj: 'Order delete successfully' });
      } else {
        return res
          .status(404)
          .send({ state: 'Not found', msj: 'Order not found' });
      }
    })
    .catch((error) => {
      console.log(error);
      return res
        .status(502)
        .send({ state: 'error', msj: 'ERROR: Bad Gateway' });
    });
});

export default orderRouter;
