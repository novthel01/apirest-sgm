import mongoose from 'mongoose';

export const connectToMongoDB = async () => {
  try {
    console.log('connecting to db...');
    mongoose.set('strictQuery', false);
    mongoose.connect(process.env.MONGODB_URI, {});
    console.log('Connected to the database');
  } catch (e) {
    console.log('database connection error');
    console.log(e);
  }
};

export const closeMongoDB = async () => {
  if (mongoose.connection) {
    try {
      console.log('closing connection...');
      mongoose.connection.close();
      console.log('closed connection');
    } catch (e) {
      console.log('error closing connection');
      console.log(e);
    }
  }
};

export const subscribeCloseMongoDB = () => {
  process.on('exit', closeMongoDB);
  process.on('SIGINT', closeMongoDB);
  process.on('SIGTERM', closeMongoDB);
  process.on('SIGKILL', closeMongoDB);
  process.on('uncaughtException', closeMongoDB);
};
