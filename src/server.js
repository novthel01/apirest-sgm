import express, { json, urlencoded } from 'express';
import * as dotenv from 'dotenv';
import allRouters from './routers';
import compression from 'compression';
import cors from 'cors';
import { connectToMongoDB, subscribeCloseMongoDB } from './db/db';

dotenv.config();

const app = express();
const port = process.env.PORT || 8000;

// configuracion middleware

app.use(cors());
app.use(json());
app.use(urlencoded({ extended: false }));
app.use(compression());

// establish connection to mongoDB
connectToMongoDB();

allRouters(app);

// configure mongoDB connection closure
subscribeCloseMongoDB();

app.listen(port, () => {
  console.log(`Servidor esta corriendo en el puerto ${port}`);
});
