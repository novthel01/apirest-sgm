import mongoose, { Schema } from 'mongoose';
import { getNextSequenceValue, inserCounter } from './Counter.js';

const notificationSchema = new Schema({
  notificationnumber: {
    type: Number,
    unique: true,
    required: [true, 'Order N° is required'],
  },

  requestby: {
    type: String,
    required: [true, 'requestby is required'],
  },

  area: {
    type: String,
    required: [true, 'Area is required'],
    max: [100, 'Area must have a maximum of 100 characters.'],
  },

  areasupervisor: {
    type: String,
    required: [true, 'Area supervisor is required'],
    max: [150, 'Area supervisor must have a maximum of 150 characters.'],
  },

  requirementdate: {
    type: Date,
    required: [true, 'Date is required'],
    default: new Date(),
  },

  machine: {
    type: String,
    required: [true, 'Machine is required'],
    max: [200, 'Machine must have a maximum of 200 characters.'],
  },

  priority: {
    type: String,
    required: [true, 'Priority is required'],
    max: [50, 'Priority must have a maximum of 50 characters.'],
  },

  status: {
    type: String,
    required: [true, 'Status is required'],
    default: 'pending',
  },

  descriptionproblem: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.'],
    required: [true, 'Description is required'],
  },
});

notificationSchema.pre('save', function (next) {
  const doc = this;
  getNextSequenceValue('notification.id')
    .then((counter) => {
      if (!counter) {
        inserCounter('notification.id')
          .then((counter) => {
            doc.notificationnumber = counter;
            next();
          })
          .catch((error) => next(error));
      } else {
        doc.notificationnumber = counter;
        next();
      }
    })
    .catch((error) => next(error));
});

const NotificationModel = mongoose.model('notification', notificationSchema);

export default NotificationModel;
