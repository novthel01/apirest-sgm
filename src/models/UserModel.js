import { genSalt, hash } from 'bcrypt';
import mongoose, { Schema } from 'mongoose';
import { getNextSequenceValue, inserCounter } from './Counter';

const userSchema = new Schema({
  code: {
    type: Number,
    unique: true,
    required: [true, 'Code N° is required'],
  },

  names: {
    type: String,
    required: [true, 'name is required'],
    max: [100, 'name must have a maximum of 100 characters.'],
  },

  lastnames: {
    type: String,
    required: [true, 'lastnames is required'],
    max: [100, 'lastnames must have a maximum of 100 characters.'],
  },

  position: {
    type: String,
    required: [true, 'position is required'],
    max: [150, 'position must have a maximum of 150 characters.'],
  },

  area: {
    type: String,
    required: [true, 'area is required'],
    max: [100, 'area must have a maximum of 100 characters.'],
  },

  phone: {
    type: Number,
    required: [true, 'Phone is required'],
  },

  email: {
    type: String,
    required: [true, 'Email is required'],
  },

  username: {
    type: String,
    required: [true, 'username is required'],
    max: [24, 'username must have a maximum of 24 characters.'],
    min: [3, 'username must have a minimum of 3 characters'],
    unique: true,
  },

  password: {
    type: String,
    required: [true, 'Password is required'],
  },

  role: {
    type: String,
    required: [true, 'Role is required'],
  },
});

userSchema.pre('save', async function (next) {
  const doc = this;
  if (!doc.isModified('password')) return next();
  const salt = await genSalt(+process.env.BCRYPT_ROUNDS);
  doc.password = await hash(doc.password, salt);
  await getNextSequenceValue('user.id')
    .then(async (counter) => {
      if (!counter) {
        await inserCounter('user.id')
          .then((counter) => {
            doc.code = counter;
            next();
          })
          .catch((error) => next(error));
      } else {
        doc.code = counter;
        next();
      }
    })
    .catch((error) => next(error));
  next();
});

const UserModel = mongoose.model('user', userSchema);

export default UserModel;
