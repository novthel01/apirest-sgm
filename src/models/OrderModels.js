import mongoose, { Schema } from 'mongoose';
import { getNextSequenceValue, inserCounter } from './Counter.js';

const orderSchema = new Schema({
  ordernumber: {
    type: Number,
    unique: true,
    required: [true, 'Order N° is required'],
  },

  priority: {
    type: String,
    required: [true, 'Priority is required'],
    max: [50, 'Priority must have a maximum of 50 characters.'],
  },

  requestby: {
    type: String,
    required: [true, 'Request by is required'],
    max: [150, 'Request by must have a maximum of 150 characters.'],
  },

  area: {
    type: String,
    required: [true, 'Area is required'],
    max: [100, 'Area must have a maximum of 100 characters.'],
  },

  activity: {
    type: String,
    required: [true, 'Activity is required'],
    max: [300, 'Activity must have a maximum of 300 characters.'],
  },

  createby: {
    type: Schema.ObjectId,
    ref: 'users',
  },

  asignateto: {
    type: Schema.ObjectId,
    ref: 'users',
  },

  spareparts: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.'],
  },

  comments: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.'],
  },

  ordercreationdate: {
    type: Date,
    required: [true, 'Date is required'],
  },

  startdate: {
    type: Date,
    required: [true, 'Date is required'],
  },

  enddate: {
    type: Date,
  },

  descriptionproblem: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.'],
    required: [true, 'Description is required'],
  },

  areasupervisor: {
    type: String,
    required: [true, 'Area supervisor is required'],
    max: [150, 'Area supervisor must have a maximum of 150 characters.'],
  },

  machine: {
    type: String,
    required: [true, 'Machine is required'],
    max: [250, 'Machine must have a maximum of 250 characters.'],
  },

  requirementdate: {
    type: Date,
    required: [true, 'Date is required'],
  },

  orderstatus: {
    type: String,
    required: [true, 'Status is required'],
    default: 'pending',
  },
});

orderSchema.pre('save', function (next) {
  const doc = this;
  getNextSequenceValue('order.id')
    .then((counter) => {
      if (!counter) {
        inserCounter('order.id')
          .then((counter) => {
            doc.ordernumber = counter;
            next();
          })
          .catch((error) => next(error));
      } else {
        doc.ordernumber = counter;
        console.log(doc.ordernumber);
        next();
      }
    })
    .catch((error) => next(error));
});

const OrderModel = mongoose.model('order', orderSchema);

export default OrderModel;
