import mongoose, { Schema } from 'mongoose';

const requestHelpSchema = new Schema({
  requestby: {
    type: Schema.ObjectId,
    ref: 'users',
  },

  status: {
    type: String,
    required: [true, 'Priority is required'],
    default: 'pending',
  },

  priority: {
    type: String,
    required: [true, 'Priority is required'],
    max: [50, 'Priority must have a maximum of 50 characters.'],
  },

  descriptionproblem: {
    type: String,
    max: [2000, 'Description must have a maximum of 2000 characters.'],
    required: [true, 'Description is required'],
  },

  requirementdate: {
    type: Date,
    required: [true, 'Date is required'],
    default: new Date(),
  },
});

const RequestHelpModel = mongoose.model('requestHelp', requestHelpSchema);

export default RequestHelpModel;
