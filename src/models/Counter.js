import mongoose, { Schema } from 'mongoose';

const CounterSchema = new Schema({
  _id: {
    type: String,
    required: true,
  },

  seq: {
    type: Number,
    default: 0,
  },
});

export const Counter = mongoose.model('counter', CounterSchema);

export const getNextSequenceValue = (seqName) => {
  return new Promise((resolve, reject) => {
    Counter.findByIdAndUpdate({ _id: seqName }, { $inc: { seq: 1 } })
      .then(function (counter) {
        if (counter) {
          resolve(counter.seq + 1);
        } else {
          resolve(null);
        }
      })
      .catch(function (error) {
        if (error) {
          reject(error);
        }
      });
  });
};

export const inserCounter = (seqName) => {
  const newCounter = new Counter({ _id: seqName, seq: 1000 });
  return new Promise((resolve, reject) => {
    newCounter
      .save()
      .then((data) => {
        resolve(data.seq);
      })
      .catch((error) => reject(error));
  });
};
