import mongoose, { Schema } from 'mongoose';

const companySchema = new Schema({
  names: {
    type: String,
    required: [true, 'name is required'],
    max: [100, 'name must have a maximum of 100 characters.'],
  },

  lastnames: {
    type: String,
    required: [true, 'lastnames is required'],
    max: [100, 'lastnames must have a maximum of 100 characters.'],
  },

  docUser: {
    type: Number,
    required: [true, 'Document is required'],
  },

  phone: {
    type: Number,
    required: [true, 'Phone is required'],
  },

  position: {
    type: String,
    required: [true, 'position is required'],
    max: [150, 'position must have a maximum of 150 characters.'],
  },

  email: {
    type: String,
    required: [true, 'Email is required'],
  },

  businessName: {
    type: String,
    required: [true, 'Name is required'],
    unique: true,
  },

  nit: {
    type: Number,
    required: [true, 'Nit is required'],
  },

  foundation: {
    type: Date,
    required: [true, 'Date is required'],
  },

  sector: {
    type: String,
    required: [true, 'name is required'],
    max: [100, 'name must have a maximum of 100 characters.'],
  },

  companyEmail: {
    type: String,
    required: [true, 'Email is required'],
  },

  companyPhone: {
    type: Number,
    required: [true, 'Phone is required'],
  },

  companyCellPhone: {
    type: Number,
    required: [true, 'Phone is required'],
  },

  authorize: {
    type: Boolean,
    default: false,
  },
});

const CompanyModel = mongoose.model('company', companySchema);

export default CompanyModel;
