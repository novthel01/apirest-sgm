import pkg from 'jsonwebtoken';
const { sign } = pkg;

const getTokenPair = async (user) => {
  const userData = {
    username: user.username,
    id: user._id,
    role: user.role,
    names: user.names,
    lastnames: user.lastnames,
  };

  const accessToken = sign(userData, process.env.JWT_ACCESS_SECRET, {
    expiresIn: '1d',
  });

  const refreshToken = sign({ id: user._id }, process.env.JWT_REFRESH_SECRET, {
    expiresIn: '7d',
  });
  return { refreshToken, accessToken };
};

export default getTokenPair;
